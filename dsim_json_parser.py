# dsim_parser.py
# python 3.11
# Last updated: 5/22/2023

# requires file structure
#   .
#   -> dsim_json_parser.py
#   -> json
#       -> load_instructions
#       -> environments
#       -> devices
#           -> drones
#           -> sensors
#           -> actuators


import os
import json

from dsim_drone import *
from dsim_environment import *


def list_files(directory: str):
    dir_list = os.listdir(directory)
    dir_string = ""
    print("--- List of json files ---")
    for f in dir_list:
        dir_string += f + " "
        print('\033[92m' + f + '\033[0m')
    print("--- End of list ---")

    return dir_string


def get_valid_input(options: str):
    options_list = options.split(" ")

    while True:
        text = input()

        if text in options_list:
            return text
        else:
            print("Not a valid option... \n" + options)


def load_env_from_file(file_loc: str):
    # Make json python recognizable
    with open(file_loc, "r") as json_file:
        json_object = json.load(json_file)

    # Try to load environment data from file_loc
    if "environment" in json_object:
        return Environment(json_object["environment"]["width"],
                           json_object["environment"]["length"],
                           json_object["environment"]["height"])
    else:
        return False


def load_drone(file_loc: str):
    with open(file_loc, "r") as json_file:
        json_object = json.load(json_file)

    if "drone" in json_object:
        drone = Drone(json_object["drone"]["battery_max"],
                      json_object["drone"]["battery_move_cost"],
                      json_object["drone"]["speed"])
        return drone
    else:
        print("No drone object found in " + file_loc)
        return False


def list_files_in_dir(directory: str):
    dir_list = os.listdir(directory)
    print("--- List of json files ---")
    for f in dir_list:
        print('\033[92m' + f + '\033[0m')
    print("--- End of list ---")
    return


def get_load_instruction_file(directory: str):
    dir_list = os.listdir(directory)

    valid = False
    while not valid:
        specified = input()
        if specified in dir_list:
            valid = True
            return specified
        else:
            print("Not a valid option")


def get_env_files_from_load_instr(directory: str):
    env_files = []

    with open(directory, "r") as instr_file:
        instr_json = json.load(instr_file)

    for filename in instr_json["environment_files"]:
        env_files.append(filename)

    return env_files


def get_drone_files_from_load_instr(directory: str):
    drone_files = []

    with open(directory, "r") as instr_file:
        instr_json = json.load(instr_file)

    for filename in instr_json["drone_files"]:
        drone_files.append(filename)

    return drone_files


def get_environment_from_directory(directory: str):
    with open(directory, "r") as env_file:
        env_json = json.load(env_file)

    blocked_cells = []
    for cell in env_json["environment"]["blocked_cells"]:
        blocked_cells.append(Position(cell["X"], cell["Y"], cell["Z"]))

    return Environment(env_json["environment"]["width"],
                       env_json["environment"]["length"],
                       env_json["environment"]["height"],
                       blocked_cells)


def get_drone_from_directory(directory: str):
    with open(directory, "r") as drone_file:
        drone_json = json.load(drone_file)

    init_pos = drone_json["drone"]["init_pos"]
    targ_pos = drone_json["drone"]["targ_pos"]

    return Drone(drone_json["drone"]["battery_max"],
                 drone_json["drone"]["battery_move_cost"],
                 drone_json["drone"]["speed"],
                 Position(init_pos["X"], init_pos["Y"], init_pos["Z"]),
                 Position(targ_pos["X"], targ_pos["Y"], targ_pos["Z"]))


# Testing stuff below
"""def parser_main():
    # Prompt load instructions
    instr_list = list_files("./json/load_instructions")
    instr_file_name = "./json/load_instructions/" + get_valid_input(instr_list)

    # Load json instructions to be python recognizable
    with open(instr_file_name, "r") as instr_file:
        instr_json = json.load(instr_file)

    # Get env_file from instr
    if "environment_file" in instr_json:
        env = load_env_from_file("json/environments/" + instr_json["environment_file"])

    # Populate drones array
    drones = []
    for drone_file in instr_json["drone_files"]:
        drones.append(load_drone("json/devices/drones/" + drone_file))

    for drone in drones:
        print(drone)

    # Column
    n = 10

    # Row
    m = 5

    # Creates an n by m array instanced with values False
    ar = [[None for i in range(n)] for j in range(m)]

    # Convert to array of indexes?
    # -1 blocked
    # 0 empty
    # index + 1 for Drones occupying cells?

    ar[2][3] = Drone(1, 1, 1)

    print(ar)

    # Print array contents
    for i in range(m):
        s = ""
        for j in range(n):
            if not ar[i][j]:
                s += str(0)
            elif isinstance(ar[i][j], Drone):
                s += str(2)
            else:
                s += str(1)
        print(s)

    print("done")

    return"""
