# dsim_environment.py
# Last updated: 5/22/2023

from dsim_drone import *



class Environment:
    """
    Environment Class

    Attributes
    -----
    width : int
        Width in number of cells
    length : int
        Length in number of cells
    height : int
        Height in number of cells
    """

    def __init__(self, width, length, height, blocked_cells: list[Position]):
        self.width = width
        self.length = length
        self.height = height
        self.blocked_cells = blocked_cells
        self.drone_list = []

        self.space = [[None for i in range(self.width)] for j in range(self.length)]
        for cell in blocked_cells:
            self.space[cell.x][cell.y] = True

    def __str__(self):
        stringified = ""

        stringified += "Width: " + str(self.width) + "\n"
        stringified += "Length: " + str(self.length) + "\n"
        stringified += "Height: " + str(self.height) + "\n"

        for y in range(self.length):
            for x in range(self.width):
                filled = False #set initial state of the cell to False
                for drone in self.drone_list:
                    if drone.position.x == x and drone.position.y == y: #if current index = drone position
                        stringified += "■" 
                        filled = True #change state to filled
                        break
                if not filled: #cells without a drone
                    if self.space[x][y]: #when cell set to True (blocked)
                        stringified += "X"  
                    else: #everywhere else
                        stringified += "·" 
                stringified += " "
            stringified += "\n"

        return stringified

    def is_in_bounds(self, position: Position):
        if 0 <= position.x < self.width and 0 <= position.y < self.length and 0 <= position.z < self.height:
            return True
        return False

    def is_cell_open(self, cell_pos: Position):
        if self.space[cell_pos.x][cell_pos.y]:
            return False
        return True

    def get_surrounding_cells(self, cell_pos: Position):
        cells = []

        forward = Position(cell_pos.x, cell_pos.y + 1, cell_pos.z)
        right = Position(cell_pos.x + 1, cell_pos.y, cell_pos.z)
        backward = Position(cell_pos.x, cell_pos.y - 1, cell_pos.z)
        left = Position(cell_pos.x - 1, cell_pos.y, cell_pos.z)

        # Forward cell
        if self.is_in_bounds(forward) and self.is_cell_open(forward):
            cells.append(forward)

        # Right cell
        if self.is_in_bounds(right) and self.is_cell_open(right):
            cells.append(right)

        # Backward cell
        if self.is_in_bounds(backward) and self.is_cell_open(backward):
            cells.append(backward)

        # Left cell
        if self.is_in_bounds(left) and self.is_cell_open(left):
            cells.append(left)

        return cells

    def best_cell(self, cells: list[Position], target_pos: Position):
        distance_list = []

        # Save default 0th cell
        best_cell = cells[0]
        best_magn = math.sqrt(
                (target_pos.x - best_cell.x) ** 2 + (target_pos.y - best_cell.y) ** 2 + (target_pos.z - best_cell.z) ** 2)

        # Iterate through checking if any cells have short distances
        for i in cells:
            distance = math.sqrt(
                (target_pos.x - i.x) ** 2 + (target_pos.y - i.y) ** 2 + (target_pos.z - i.z) ** 2)
            if distance < best_magn:
                best_cell = i
                best_magn = distance

        # Return best cell
        return best_cell

    def tick_drone(self, drone: Drone):
        #check if drone met target
        if drone.position == drone.targ_pos:
            return False

        # Check battery
        if drone.battery_cur < drone.battery_move_cost:
            return False

        # Get surrounding cells
        surrounding_cells = self.get_surrounding_cells(drone.position)

        # if no cells return false to be removed from list
        if len(surrounding_cells) <= 0:
            return False

        # Remove closed cells
        for cell in surrounding_cells:
            if not self.is_cell_open(cell):
                surrounding_cells.remove(cell)

        # Default fill next_cell
        """next_cell = surrounding_cells[0]
        next_magn = next_cell.distance_to(drone.targ_pos)

        # Iterate and check if better option
        for cell in surrounding_cells:
            cell_magn = cell.distance_to(drone.targ_pos)
            if cell_magn < next_magn:
                next_magn = cell_magn
                next_cell = cell
        """

        # Get best cell
        next_cell = self.best_cell(surrounding_cells, drone.targ_pos)

        # Reduce battery
        drone.battery_cur -= drone.battery_move_cost

        # Remove drone from previous cell
        self.space[drone.position.x][drone.position.y] = False

        # Move drone to next cell
        self.space[next_cell.x][next_cell.y] = drone

        # Update drone self position
        drone.position = next_cell

        return
