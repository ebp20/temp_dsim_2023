# dsim_main.py
# Last updated: 5/22/2023


from dsim_json_parser import *

JSON_DIR = "./json/"
LOAD_INSTR_DIR = "./json/load_instructions/"
DRONE_DIR = "./json/devices/drones/"
ENVIRONMENT_DIR = "./json/environments/"


def main():
    # Print all load instruction json's
    list_files_in_dir(LOAD_INSTR_DIR)

    # Get load instruction file
    print("Please enter a load instruction file... ")
    load_instr = get_load_instruction_file(LOAD_INSTR_DIR)

    # Get lists of file names
    env_files = get_env_files_from_load_instr(LOAD_INSTR_DIR + load_instr)
    drone_files = get_drone_files_from_load_instr(LOAD_INSTR_DIR + load_instr)

    print(env_files)
    print(drone_files)

    # Convert env_file into environment object
    environment = get_environment_from_directory(ENVIRONMENT_DIR + env_files[0])

    # Multiple environment set up
    """environments = []
    for filename in env_files:
        environments.append(get_environment_from_directory(ENVIRONMENT_DIR + filename))
    """

    # Compile list of drone objects
    drone_protos = []
    for filename in drone_files:
        drone_protos.append(get_drone_from_directory(DRONE_DIR + filename))

    # Inject drones into environment
    for drone in drone_protos:
        if not environment.space[drone.position.x][drone.position.y]:
            environment.drone_list.append(drone)
            environment.space[drone.position.x][drone.position.y] = drone
        else:
            print("Cell occupied")

    # DEBUG Display contents of lists
    print(environment)

    # Line break
    print("**********")

    #loop for multiple drones
    all_drones_reached_target = False
    while not all_drones_reached_target:
        all_drones_reached_target = True  # initially to set True to end loop if all targets met
        for drone in environment.drone_list:
            if not drone.position == drone.targ_pos: # if drone has not yet met target
                all_drones_reached_target = False  # set back to False to keep running loop until targets met
                environment.tick_drone(drone)
                print(environment)

    print("DONE")


# BEGIN MAIN PROGRAM
main()
