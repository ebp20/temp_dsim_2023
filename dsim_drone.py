# dsim_drone.py
# Last updated : 5/22/2023
import math
from enum import Enum


class Position:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def __eq__(self, other):
        if self.x == other.x and self.y == other.y and self.z == other.z:
            return True
        else:
            return False

    def distance_to(self, pos_b):
        distance = math.sqrt((pos_b.x - self.x) ** 2 + (pos_b.y - self.y) ** 2)
        return distance
    


class Mode(Enum):
    GOTO = 1            # Moves form start to target
    GOTO_RETURN = 2     # Moves from start to target, then returns to start
    LOOP = 3            # Continually moves from start to target, swapping both upon reaching target


class Drone:
    """
    Drone Class

    Attributes
    -----
    battery_max : int
        Maximum battery in mAh
    battery_move_cost : int
        Amount of mAh taken from battery to move drone
    speed : int
        Number of cells a drone can move in one update
    """

    # Constructor
    def __init__(self, battery_max: int, battery_move_cost: int, speed: int, init_pos: Position, targ_pos: Position):
        self.battery_max = battery_max
        self.battery_cur = self.battery_max
        self.battery_move_cost = battery_move_cost
        self.speed = speed

        self.init_pos = init_pos
        self.position = Position(init_pos.x, init_pos.y, init_pos.z)
        self.targ_pos = targ_pos

    def __str__(self):
        stringified = ""

        stringified += "battery_max: " + str(self.battery_max) + "\n"
        stringified += "battery_move_cost: " + str(self.battery_move_cost) + "\n"
        stringified += "speed: " + str(self.speed)

        return stringified
